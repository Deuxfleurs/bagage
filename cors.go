package main

import (
	"net/http"
)

type CorsAllowAllOrigins struct {
	AndThen http.Handler
}

func (c CorsAllowAllOrigins) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Allow-Methods", "*")
	w.Header().Add("Access-Control-Allow-Headers", "*")
	c.AndThen.ServeHTTP(w, r)
}

type OptionsNoError struct {
	Error ErrorHandler
}

func (c OptionsNoError) WithError(err error) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			w.WriteHeader(200)
		} else {
			c.Error.WithError(err).ServeHTTP(w, r)
		}
	})
}
