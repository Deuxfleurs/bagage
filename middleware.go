package main

import (
	"github.com/minio/minio-go/v7"
	"net/http"
)

/* We define some interface to enable our middleware to communicate */
type ErrorHandler interface {
	WithError(err error) http.Handler
}
type CredsHandler interface {
	WithCreds(username, password string) http.Handler
}
type MinioClientHandler interface {
	WithMC(mc *minio.Client) http.Handler
}
