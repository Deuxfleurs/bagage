package main

import (
	"git.deuxfleurs.fr/Deuxfleurs/bagage/s3"
	"github.com/minio/minio-go/v7"
	"golang.org/x/net/webdav"
	"log"
	"net/http"
)

type WebDav struct {
	WithConfig *Config
}

func (wd WebDav) WithMC(mc *minio.Client) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		(&webdav.Handler{
			Prefix:     wd.WithConfig.DavPath,
			FileSystem: s3.NewS3FS(mc, wd.WithConfig.S3Cache),
			LockSystem: webdav.NewMemLS(),
			Logger: func(r *http.Request, err error) {
				log.Printf("INFO: %s %s %s\n", r.RemoteAddr, r.Method, r.URL)
				if err != nil {
					log.Printf("ERR: %v", err)
				}
			},
		}).ServeHTTP(w, r)
	})
}
