module git.deuxfleurs.fr/Deuxfleurs/bagage

go 1.16

require (
	github.com/go-ldap/ldap/v3 v3.4.1
	github.com/kr/fs v0.1.0
	github.com/minio/minio-go/v7 v7.0.12
	github.com/pkg/sftp v1.13.4
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d
)
