package main

import (
	"net/http"
)

type NotAuthorized struct{}

func (n NotAuthorized) WithError(err error) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("WWW-Authenticate", `Basic realm="Pour accéder à Bagage, veuillez entrer vos identifiants Deuxfleurs"`)
		w.WriteHeader(401)
		w.Write([]byte("401 Unauthorized\n"))
	})
}

type InternalError struct{}

func (i InternalError) WithError(err error) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(500)
		w.Write([]byte("500 Internal Server Error\n"))
	})
}
